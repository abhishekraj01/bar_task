package com.musejam.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class OrderAddOnItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long orderAddOnItemId;
    
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "productId")
    private Product product;

    @Column(name = "productId", insertable = false, updatable = false)
    private Long productId;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinColumn(name = "orderItemId")
    private OrderItem orderItem;

    @Column(name = "orderItemId", insertable = false, updatable = false)
    private Long orderItemId;

	public Long getOrderAddOnItemId() {
		return orderAddOnItemId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public OrderItem getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(OrderItem orderItem) {
		this.orderItem = orderItem;
	}

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public void setOrderAddOnItemId(Long orderAddOnItemId) {
		this.orderAddOnItemId = orderAddOnItemId;
	}

	@Override
	public String toString() {
		return "OrderAddOnItem [orderAddOnItemId=" + orderAddOnItemId + ", product=" + product + ", productId="
				+ productId + ", orderItem=" + orderItem + ", orderItemId=" + orderItemId + ", getOrderAddOnItemId()="
				+ getOrderAddOnItemId() + ", getProduct()=" + getProduct() + ", getProductId()=" + getProductId()
				+ ", getOrderItem()=" + getOrderItem() + ", getOrderItemId()=" + getOrderItemId() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	

}
