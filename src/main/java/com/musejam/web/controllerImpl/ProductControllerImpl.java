package com.musejam.web.controllerImpl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.musejam.persistence.Product;
import com.musejam.service.ProductService;
import com.musejam.web.controller.ProductController;

//@CrossOrigin(origins = "http://localhost:80880", maxAge = 3600)
@Controller
public class ProductControllerImpl implements ProductController {
	
	@Autowired
	private ProductService productService;

	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public Product saveProduct(@RequestBody Product product) {
		Product saveProduct = productService.saveProduct(product);
		return saveProduct;
	}

	@RequestMapping(value = "/all/product", method = RequestMethod.GET)
	public List<Product> allProduct() {
		List<Product> allProduct = productService.allProduct();
		return allProduct;
	}

	@RequestMapping(value = "/product/{productId}", method = RequestMethod.GET)
	public Product findById(@PathVariable Long productId) {
		Product product = productService.findById(productId);
		return product;
	}

	@RequestMapping(value = "/product", method = RequestMethod.DELETE)
	public void deleteProduct(@PathVariable Long productId) {
		productService.deleteProduct(productId);
	}

	@RequestMapping(value = "/count/product", method = RequestMethod.GET)
	public int countAllProduct() {
		List<Product> product = productService.allProduct();
		int size = product.size();
		return size;
	}

	
}
