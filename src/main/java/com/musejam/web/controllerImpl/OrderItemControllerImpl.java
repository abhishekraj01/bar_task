package com.musejam.web.controllerImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;

import com.musejam.persistence.OrderItem;
import com.musejam.service.OrderItemService;
import com.musejam.web.controller.OrderItemController;

//@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)

public class OrderItemControllerImpl implements OrderItemController {
	@Autowired
	private OrderItemService orderItemService;

	@RequestMapping(value = "/orderItem", method = RequestMethod.POST)
	public OrderItem saveOrder(@RequestBody OrderItem orderItem) {
		OrderItem saveOrderItem = orderItemService.saveOrder(orderItem);
		return saveOrderItem;
	}

	@RequestMapping(value = "/orderItem", method = RequestMethod.GET)
	public List<OrderItem> allOrderItem() {
		List<OrderItem> allOrderItm = orderItemService.allOrderItem();
		return allOrderItm;
	}

	@RequestMapping(value = "/orderItem/{orderItemId}", method = RequestMethod.GET)
	public OrderItem findById(@PathVariable Long orderItemId) {
		OrderItem orderItem = orderItemService.findById(orderItemId);
		return orderItem;
	}

	@RequestMapping(value = "/orderItem/{orderItemId}", method = RequestMethod.DELETE)
	public void deleteOrderItem(@PathVariable Long orderItemId) {
		orderItemService.deleteOrderItem(orderItemId);
		
	}

	@RequestMapping(value = "/count/orderItem", method = RequestMethod.GET)
	public int countAllOrder() {
		List<OrderItem> allOrderItm = orderItemService.allOrderItem();
		int size = allOrderItm.size();
		return size;
	}

	
}
