package com.musejam.web.controllerImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.musejam.persistence.OrderAddOnItem;
import com.musejam.service.OrderAddOnItemService;
import com.musejam.web.controller.OrderAddOnItemController;

@Controller
public class OrderAddOnItemControllerImpl implements OrderAddOnItemController{
	
	@Autowired
	private OrderAddOnItemService orderAddOnItemService;

	@RequestMapping(value = "/orderAddOnItem", method = RequestMethod.POST)
	public OrderAddOnItem saveOrderAddOnItem(@RequestBody OrderAddOnItem orderAddOnItem) {
		OrderAddOnItem saveOrderAddOnItem = orderAddOnItemService.saveOrderAddOnItem(orderAddOnItem);
		return saveOrderAddOnItem;
	}

	@RequestMapping(value = "/orderAddOnItem", method = RequestMethod.GET)
	public List<OrderAddOnItem> allOrderAddOnItem() {
		List<OrderAddOnItem> allOrderAddOnItem = orderAddOnItemService.allOrderAddOnItem();
		return allOrderAddOnItem;
	}

	@RequestMapping(value = "/orderAddOnItem/{orderAddOnItemId}", method = RequestMethod.GET)
	public OrderAddOnItem findById(@PathVariable Long orderAddOnItemId) {
		OrderAddOnItem orderAddOnItm = orderAddOnItemService.findById(orderAddOnItemId);
		return orderAddOnItm;
	}

	@RequestMapping(value = "/orderAddOnItem/{orderAddOnItemId}", method = RequestMethod.DELETE)
	public void deleteOrderAddOnItem(Long orderAddOnItemId) {
		orderAddOnItemService.deleteOrderAddOnItem(orderAddOnItemId);
	}

	@RequestMapping(value = "/count/orderAddOnItem", method = RequestMethod.GET)
	public int countAllOrderAddOnItem() {
		List<OrderAddOnItem> allOrderAddOnItem = orderAddOnItemService.allOrderAddOnItem();
		int count = allOrderAddOnItem.size();
		return count;
	}

	
}
