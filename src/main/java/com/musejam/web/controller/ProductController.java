package com.musejam.web.controller;

import java.util.List;

import com.musejam.persistence.Product;

	public interface ProductController {
	
	public Product saveProduct(Product product);

	public List<Product> allProduct();

	public Product findById(Long productId);

	public void deleteProduct(Long productId);

	public int countAllProduct();

//	public Product updateProduct(Long productId);
	

}
