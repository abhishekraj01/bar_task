package com.musejam.web.controller;

import java.util.List;

import com.musejam.persistence.OrderAddOnItem;

public interface OrderAddOnItemController {

	public OrderAddOnItem saveOrderAddOnItem(OrderAddOnItem orderAddOnItem);
	
	public List<OrderAddOnItem> allOrderAddOnItem();
	
	public OrderAddOnItem findById(Long orderAddOnItemId);
	
	public void deleteOrderAddOnItem(Long orderAddOnItemId);
	
	public int countAllOrderAddOnItem();	
	
//	public OrderItem updateOrderAddOnItem(Long orderAddOnItemId);
	
}
