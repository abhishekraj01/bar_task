package com.musejam.web.controller;

import java.util.List;

import com.musejam.persistence.OrderItem;

public interface OrderItemController {

	public OrderItem saveOrder(OrderItem orderItem);
	
	public List<OrderItem> allOrderItem();
	
	public OrderItem findById(Long orderItemId);
	
	public void deleteOrderItem(Long orderItemId);
	
	public int countAllOrder();
	
//	public OrderItem updateOrderItem(Long orderItemId);
}
