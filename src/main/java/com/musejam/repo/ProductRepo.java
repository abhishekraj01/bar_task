package com.musejam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musejam.persistence.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

}
