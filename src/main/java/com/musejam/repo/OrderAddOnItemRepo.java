package com.musejam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musejam.persistence.OrderAddOnItem;

public interface OrderAddOnItemRepo extends JpaRepository<OrderAddOnItem, Long> {
	

}
