package com.musejam.service;

import java.util.List;

import com.musejam.persistence.OrderAddOnItem;
import com.musejam.repo.OrderAddOnItemRepo;

public interface OrderAddOnItemService {

public OrderAddOnItem saveOrderAddOnItem(OrderAddOnItem orderAddOnItem);
	
	public List<OrderAddOnItem> allOrderAddOnItem();
	
	public OrderAddOnItem findById(Long orderAddOnItemId);
	
	public void deleteOrderAddOnItem(Long orderAddOnItemId);
	
	public int countAllOrderAddOnItem();	
	
//	public OrderItem updateOrderAddOnItem(Long orderAddOnItemId);
	
}
