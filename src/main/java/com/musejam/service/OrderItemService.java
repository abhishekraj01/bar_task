package com.musejam.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.musejam.persistence.OrderItem;
@Repository
public interface OrderItemService {

	public OrderItem saveOrder(OrderItem orderItem);
	
	public List<OrderItem> allOrderItem();
	
	public OrderItem findById(Long orderItemId);
	
	public void deleteOrderItem(Long orderItemId);
	
	public int countAllOrder();
	
//	public OrderItem updateOrderItem(Long orderItemId);
	
}
