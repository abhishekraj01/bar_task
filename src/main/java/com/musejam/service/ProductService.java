package com.musejam.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.musejam.persistence.OrderItem;
import com.musejam.persistence.Product;

public interface ProductService {
	
	public List<Product> allProduct();

	public Product findById(Long productId);

	public void deleteProduct(Long productId);

	public int countAllProduct();

	public Product saveProduct(Product product);

//	public Product updateProduct(Long productId);

}
