package com.musejam.serviceImpl;

import java.util.List;

import com.musejam.persistence.OrderAddOnItem;
import com.musejam.persistence.OrderItem;
import com.musejam.repo.OrderAddOnItemRepo;
import com.musejam.service.OrderAddOnItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.util.Assert.notNull;

@Service
public class OrderAddOnItemImpl implements OrderAddOnItemService {

	@Autowired
	private OrderAddOnItemRepo orderAddOnItemRepo;

	public OrderAddOnItem saveOrderAddOnItem(OrderAddOnItem orderAddOnItem) {
		OrderAddOnItem saveOrderAddOnItem = orderAddOnItemRepo.save(orderAddOnItem);
		return saveOrderAddOnItem;
	}

	public List<OrderAddOnItem> allOrderAddOnItem() {
		List<OrderAddOnItem> allOrderAddOnItem = orderAddOnItemRepo.findAll();
		return allOrderAddOnItem;
	}

	public OrderAddOnItem findById(Long orderAddOnItemId) {
		OrderAddOnItem orderAddOnItem = orderAddOnItemRepo.findOne(orderAddOnItemId);
		return orderAddOnItem;
	}

	public void deleteOrderAddOnItem(Long orderAddOnItemId) {
		OrderAddOnItem orderAddOnItem = orderAddOnItemRepo.findOne(orderAddOnItemId);
		if(orderAddOnItem != null){
			orderAddOnItemRepo.delete(orderAddOnItemId);
		}
	}

	public int countAllOrderAddOnItem() {
		List<OrderAddOnItem> allOrderAddOnItem = orderAddOnItemRepo.findAll();
		int size = allOrderAddOnItem.size();
		return size;
	}
	
}
