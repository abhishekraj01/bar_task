package com.musejam.serviceImpl;

import java.util.List;

import com.musejam.persistence.OrderItem;
import com.musejam.repo.OrderItemRepo;
import com.musejam.repo.ProductRepo;
import com.musejam.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import static org.springframework.util.Assert.notNull;

@Service
public class OrderServiceImpl implements OrderItemService {

	@Autowired
	private OrderItemRepo orderItemRepo;

	

	public List<OrderItem> allOrderItem() {
		List<OrderItem> allOrderItem = orderItemRepo.findAll();
		return allOrderItem;
	}

	public OrderItem findById(Long orderItemId) {
		OrderItem orderItem = orderItemRepo.findOne(orderItemId);
		return orderItem;
	}

	public void deleteOrderItem(Long orderItemId) {
		OrderItem orderItem = orderItemRepo.findOne(orderItemId);
		if (orderItem != null) {
			orderItemRepo.delete(orderItemId);
		}
	}

	public int countAllOrder() {
		List<OrderItem> allOrderItem = orderItemRepo.findAll();
		int size = allOrderItem.size();
		return size;
	}

	public OrderItem updateOrderItem(Long orderItemId) {

		return null;
	}

	public OrderItem saveOrder(OrderItem orderItem) {
		OrderItem saveOrdItem = orderItemRepo.save(orderItem);
		return saveOrdItem;
	}

	
}
