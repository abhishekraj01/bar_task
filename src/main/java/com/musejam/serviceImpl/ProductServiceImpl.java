package com.musejam.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.musejam.persistence.OrderItem;
import com.musejam.persistence.Product;
import com.musejam.repo.ProductRepo;
import com.musejam.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepo productRepo;

	public Product saveProduct(Product product) {
		Product saveProduct =  productRepo.save(product);
		return saveProduct;
	}

	public List<Product> allProduct() {
		List<Product> allProduct = productRepo.findAll();
		return allProduct;
	}

	public Product findById(Long productId) {
		Product product = productRepo.findOne(productId);
		return product;
	}

	public void deleteProduct(Long productId) {
		productRepo.delete(productId);
	}

	public int countAllProduct() {
		List<Product> allProduct = productRepo.findAll();
		int size = allProduct.size();
		return size;
	}

	public Product save(Product product) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
